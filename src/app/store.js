import { configureStore } from "@reduxjs/toolkit";
import navbarReducers from "../features/navbarSlice";

export const store = configureStore({
	reducer: { navbar: navbarReducers },
});
