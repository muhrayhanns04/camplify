import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	isActive: false,
};

export const navbarSlice = createSlice({
	name: "navbar",
	initialState,
	reducers: {
		toggleButton: (state, action) => {
			state.isActive = !state.isActive;
		},
	},
});

export const { toggleButton } = navbarSlice.actions;
export const getIsActive = (state) => state.navbar.isActive;

export default navbarSlice.reducer;
