import Content from "../../components/organisms/Technical/Content";
import React from "react";

const TechnicalTestPage = () => {
	return (
		<div className="flex w-full flex-col">
			<Content />
		</div>
	);
};

export default TechnicalTestPage;
