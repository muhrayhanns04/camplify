import { Link } from "react-router-dom";
import React from "react";

const NotFoundPage = () => {
	return (
		<div className="mx-auto flex flex-col items-center overflow-hidden pt-120 lg:w-[895px]">
			<h1 className="text-42 font-bold text-black">404 - Not Found!</h1>
			<Link
				className="mt-32 rounded-full bg-blue-500 py-16 px-32 font-regular text-16 text-white hover:text-white"
				to="/"
			>
				Go Home
			</Link>
		</div>
	);
};

export default NotFoundPage;
