import About from "../../components/organisms/Home/About";
import Contact from "../../components/organisms/Home/Contact";
import Footer from "../../components/organisms/Home/Footer";
import Hero from "../../components/organisms/Home/Hero";
import Info from "../../components/organisms/Home/Info";
import React from "react";

const HomePage = () => {
	return (
		<div className="flex h-full w-full flex-col bg-white">
			<Hero />
			<Info />
			<About />
			<Contact />
			<Footer />
		</div>
	);
};

export default HomePage;
