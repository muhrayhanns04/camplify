import { Route, Routes } from "react-router-dom";

import { GlobalDebug } from "./helpers/Common";
import HomePage from "./pages/HomePage";
import MobileNavbar from "./components/organisms/MobileNavbar";
import Navbar from "./components/organisms/Navbar";
import NotFoundPage from "./pages/NotFoundPage";
import React from "react";
import TechnicalTestPage from "./pages/TechnicalTestPage";
import { getIsActive } from "./features/navbarSlice";
import { useSelector } from "react-redux";

function App() {
	React.useEffect(() => {
		GlobalDebug(false);
	}, []);
	const isActive = useSelector(getIsActive);

	return (
		<React.Fragment>
			<Navbar />
			{isActive ? (
				<MobileNavbar />
			) : (
				<Routes>
					<Route exact path="/" element={<HomePage />} />
					<Route path="*" element={<NotFoundPage />} />
					<Route path="/test" element={<TechnicalTestPage />} />
				</Routes>
			)}
		</React.Fragment>
	);
}

export default App;
