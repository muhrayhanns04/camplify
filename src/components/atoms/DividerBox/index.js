import React from "react";

const DividerBox = ({ title }) => {
	return (
		<div className="mb-16 mt-24 flex flex-row items-center rounded-full bg-blue-500 py-13 px-16">
			<h1 className="text-16 font-bold text-white">
				CASE {title.toUpperCase()}
			</h1>
		</div>
	);
};

export default DividerBox;
