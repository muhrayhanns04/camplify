import { BeatLoader } from "react-spinners";
import React from "react";

const ButtonComponent = ({
	value,
	className,
	textClass,
	isLoading,
	fitContent,
}) => {
	return (
		<button
			type="submit"
			target="_blank"
			className={`no-decoration flex w-full cursor-pointer flex-row items-center justify-center rounded-full bg-gray-900 py-16 no-underline ${className} inline-block ${
				fitContent && "w-auto px-[49px]"
			}`}
		>
			{isLoading ? (
				<BeatLoader color="#FFFFFF" size={16} />
			) : (
				<p
					className={`text-center font-semi-bold text-16 ${textClass}
 no-decoration text-white no-underline`}
				>
					{value}
				</p>
			)}
		</button>
	);
};

export default ButtonComponent;
