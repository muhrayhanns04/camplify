import React from "react";

const InfoItem = ({ item }) => {
	return (
		<div className="flex flex-row items-center justify-center">
			<h3 className="mr-16 font-semi-bold text-42 text-white">
				{item.value}
			</h3>
			<p className="w-[85px] font-regular text-16 text-white">
				{item.name}
			</p>
		</div>
	);
};

export default InfoItem;
