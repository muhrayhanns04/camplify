import { getIsActive, toggleButton } from "../../../features/navbarSlice";
import { useDispatch, useSelector } from "react-redux";

import { HamburgerSpring } from "react-animated-burgers";
import { NavLink } from "react-router-dom";
import React from "react";
import routes from "../../../api/routes.json";
import { useWindowScrollPositions } from "../../atoms/useWindowScrollPositions";

const Navbar = () => {
	const dispatch = useDispatch();
	const { scrollY } = useWindowScrollPositions();
	const isActive = useSelector(getIsActive);

	return (
		<div
			className={`fixed z-10 w-full py-13 lg:py-24 ${
				scrollY >= 91 ? "bg-blue-500" : ""
			}`}
		>
			<div className="m-auto w-90% lg:w-[895px]">
				<div className="m-auto flex flex-row items-center justify-between">
					<h1
						className={`cursor-not-allowed text-24 font-bold ${
							scrollY >= 91 ? "text-white" : "text-black"
						}`}
					>
						Camplify
					</h1>

					<div className="flex lg:hidden">
						<HamburgerSpring
							buttonWidth={24}
							barColor={scrollY <= 91 ? "black" : "white"}
							{...{
								isActive: isActive,
								toggleButton: () => dispatch(toggleButton()),
							}}
						/>
					</div>

					<div className="hidden flex-row items-center space-x-64 lg:flex">
						{routes.map((item, index) => {
							return (
								<React.Fragment key={index}>
									<NavLink to={item.path}>
										{({ isActive }) => (
											<span
												className={`font-semi-bold ${
													isActive &&
													"font-bold text-orange-500"
												} text-16 ${
													scrollY >= 91
														? "text-white hover:text-white"
														: "text-black hover:text-blue-500"
												}`}
											>
												{item.name}
											</span>
										)}
									</NavLink>
								</React.Fragment>
							);
						})}
					</div>
				</div>
			</div>
		</div>
	);
};

export default Navbar;
