import React from "react";
import fruits from "../../../api/fruits.json";

const Question = ({ item, data, i }) => {
	const numberOfContainers = fruits.reduce((result, currentValue) => {
		(result[currentValue.fruitType] =
			result[currentValue.fruitType] || []).push(currentValue);
		return result;
	}, {});

	const handleGetFruits = (typeName) => {
		const data = fruits.filter((item) => item.fruitType == typeName);
		let fruitNames = data.map((e) => e.fruitName).join(", ");
		const totalStock = data.reduce((total, item) => {
			return total + item.stock;
		}, 0);

		return {
			value: fruitNames,
			totalStock: totalStock,
		};
	};

	return (
		<div className="mb-16 flex flex-row items-center">
			<div>
				<p className="mb-8 w-full text-left text-16 font-bold text-black">
					{i + 1}. {item.value}{" "}
				</p>
				{i == 0 && (
					<article>
						<p className="w-full text-left font-regular text-16 text-gray-300">
							{data}
						</p>
					</article>
				)}
				{i == 1 && (
					<article>
						<p className="w-full text-left font-regular text-16 text-gray-300">
							Andi membutuhkan{" "}
							{Object.keys(numberOfContainers)?.length} wadah,
							untuk buah tipe
						</p>
						<p className="w-full text-left font-regular text-16 text-gray-300">
							IMPORT: {handleGetFruits("IMPORT").value}
						</p>
						<p className="w-full text-left font-regular text-16 text-gray-300">
							LOCAL: {handleGetFruits("LOCAL").value}
						</p>
					</article>
				)}
				{i == 2 && (
					<article>
						<p className="w-full text-left font-regular text-16 text-gray-300">
							Total stock untuk IMPORT sebanyak{" "}
							{handleGetFruits("IMPORT").totalStock} & LOCAL
							sebanyak {handleGetFruits("LOCAL").totalStock}
						</p>
					</article>
				)}
				{i == 3 && (
					<article>
						<p className="mb-16 w-full text-left font-regular text-16 text-gray-300">
							Menurut saya lebih baik jika fruitType di pisah
							dibuatkan data yg berbeda khusus untuk fruitTypes,
							karena dengan cara itu memudahkan kita jika ada
							perubahan nama pada fruitType, tanpa harus merubah
							nya satu pertsatu.
						</p>
						<p className="w-full text-left font-regular text-16 text-gray-300">
							Value id pada kasus diatas seharusnya bersifat
							unique alias tidak boleh ada id yang sama.
						</p>
					</article>
				)}
			</div>
		</div>
	);
};

export default Question;
