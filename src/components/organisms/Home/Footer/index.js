import React from "react";

const Footer = () => {
	return (
		<div className="w-full bg-blue-500 py-16">
			<p className="text-center font-semi-bold text-13 text-white">
				Copyright © 2022 Camplify. All Rights Reserved
			</p>
		</div>
	);
};

export default Footer;
