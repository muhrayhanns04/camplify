import ButtonComponent from "../../../atoms/ButtonComponent";
import React from "react";
import memphisOne from "../../../../assets/icons/memphis-1.svg";
import mockup from "../../../../assets/images/mockup_1.png";

const Hero = () => {
	return (
		<div className="mx-auto mt-120 flex flex-col items-center justify-between lg:w-[895px] lg:flex-row ">
			<section className="mx-auto mb-32 w-90% lg:mx-0 lg:w-[387px]">
				<div className="relative">
					<h1 className="mb-16 text-[52px] font-bold text-black">
						Cabin In The Woods But In A Good Way
					</h1>
					<figure>
						<img
							src={memphisOne}
							className="top-[-50px] right-[-20px] hidden cursor-not-allowed lg:absolute"
							alt="memphis"
						/>
					</figure>
				</div>
				<p className="mb-24 font-regular text-16 text-gray-300">
					Now you can enjoy camping anywhere and anytime and of course
					it’s safe with us.
				</p>
				<ButtonComponent
					className="button-transition"
					value="Register"
					fitContent="true"
				/>
			</section>
			<section>
				<img src={mockup} alt="mockup" className="cursor-not-allowed" />
			</section>
		</div>
	);
};

export default Hero;
