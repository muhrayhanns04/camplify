import InfoItem from "../../../molecules/Info/Item";
import React from "react";
import data from "../../../../api/realtime_data.json";

const Info = () => {
	return (
		<div className="mb-64 w-full overflow-hidden bg-blue-500 py-24 lg:py-42">
			<section className="m-auto grid w-90% grid-cols-1 content-center gap-24 lg:w-[895px] lg:grid-cols-4 lg:gap-0">
				{data.map((item, index) => (
					<React.Fragment key={index}>
						<InfoItem item={item} />
					</React.Fragment>
				))}
			</section>
		</div>
	);
};

export default Info;
