import ButtonComponent from "../../../atoms/ButtonComponent";
import React from "react";
import memphisOne from "../../../../assets/icons/memphis-1.svg";
import mockup from "../../../../assets/images/maps.png";

const About = () => {
	return (
		<div className="mx-auto flex flex-col-reverse items-center justify-between overflow-hidden lg:mb-64 lg:w-[895px] lg:flex-row">
			<figure>
				<img className="cursor-not-allowed" src={mockup} alt="mockup" />
			</figure>
			<section className="mx-auto mb-32 w-90% lg:mx-0 lg:mt-0 lg:w-[387px]">
				<h1 className="mb-16 text-32 font-bold text-black">
					Forest, Starry Night, Compfire, What Else Do You Need?
				</h1>
				<p className="font-regular text-16 text-gray-300">
					Now you can enjoy camping anywhere and anytime and of course
					it’s safe with us.
				</p>
			</section>
		</div>
	);
};

export default About;
