import { ReactComponent as Arrow } from "../../../../../assets/icons/arrow.svg";
import React from "react";

const Question = ({ item }) => {
	const [isOpen, setIsOpen] = React.useState(false);

	const handleIsOpen = () => {
		setIsOpen(!isOpen);
	};

	return (
		<React.Fragment>
			<button
				onClick={handleIsOpen}
				className={`flex w-full flex-row items-center justify-between ${
					!isOpen && "border-b-2"
				} border-b-gray-50 py-16`}
			>
				<p className="font-semi-bold text-16 text-gray-900">
					{item.title}
				</p>
				<Arrow
					stroke="#025CE0"
					className={`${!isOpen && "-rotate-90"}`}
				/>
			</button>
			{isOpen && (
				<p className=" mb-24 font-regular text-16 text-gray-300">
					{typeof item?.value == "string" && item.value}
				</p>
			)}
			{isOpen && typeof item?.value == "object" && (
				<div>
					{item?.value.map((item) => (
						<React.Fragment key={item.id}>
							<p className=" mb-24 font-regular text-16 text-gray-300">
								{item.id}. {item.step}
							</p>
						</React.Fragment>
					))}
				</div>
			)}
		</React.Fragment>
	);
};

export default Question;
