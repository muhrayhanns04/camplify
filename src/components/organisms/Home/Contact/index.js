import ButtonComponent from "../../../atoms/ButtonComponent";
import Question from "./Question";
import React from "react";
import questions from "../../../../api/questions.json";

const Contact = () => {
	return (
		<div className="mx-auto my-64 mt-32 flex flex-col items-center justify-between lg:w-[895px] lg:flex-row">
			<section className="mx-auto w-90% lg:mx-0 lg:mt-0 lg:w-[387px]">
				<h1 className="mb-24 text-32 font-bold text-black">
					Maybe your question is have been aswered, check this out
				</h1>
				<article className="flex flex-col items-start">
					{questions.map((item, index) => (
						<React.Fragment key={index}>
							<Question item={item} />
						</React.Fragment>
					))}
				</article>
			</section>
			<section className="mx-auto mt-32 w-90% lg:mx-0 lg:mt-0 lg:w-[387px]">
				<article className="mb-24">
					<h1 className="mb-16 text-32 font-bold text-black">
						Camplify
					</h1>
					<p className=" font-regular text-16 text-gray-300">
						Now you can enjoy camping anywhere and anytime and of
						course it’s safe with us.
					</p>
				</article>

				<section className="relative flex flex-row items-center">
					<input
						placeholder="Type your message"
						type="text"
						className="mr-16 w-full border-b-2 border-b-gray-900 bg-gray-50 p-16 font-regular outline-none"
					/>
					<ButtonComponent
						value="Send"
						fitContent
						className="button-transition bg-blue-500"
					/>
				</section>
			</section>
		</div>
	);
};

export default Contact;
