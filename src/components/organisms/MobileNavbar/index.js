import { getIsActive, toggleButton } from "../../../features/navbarSlice";
import { useDispatch, useSelector } from "react-redux";

import { ReactComponent as Arrow } from "../../../assets/icons/arrow.svg";
import { HamburgerSpring } from "react-animated-burgers";
import { NavLink } from "react-router-dom";
import React from "react";
import routes from "../../../api/routes.json";

const MobileNavbar = () => {
	const dispatch = useDispatch();
	const isActive = useSelector(getIsActive);

	return (
		<div
			className={`fixed z-50 flex h-[100%] w-[100%] flex-col items-center space-y-32 overflow-hidden bg-gray-800 ${
				isActive ? "" : "hidden"
			}`}
		>
			<HamburgerSpring
				buttonWidth={24}
				barColor="white"
				className="absolute right-24"
				{...{
					isActive: isActive,
					toggleButton: () => dispatch(toggleButton()),
				}}
			/>
			<div className="mt-24 flex w-[90%] flex-col lg:mb-42">
				<h3 className="mb-16 w-[80%] text-32 font-bold uppercase text-white lg:text-42">
					Camplify.
				</h3>
				<p className="w-[70%] font-semi-bold text-13 leading-[163%] text-white lg:text-24">
					Now you can enjoy camping anywhere and anytime and of course
					it’s safe with us.
				</p>
			</div>
			<div className="grid w-[90%] grid-cols-1 gap-32 lg:gap-42">
				{routes.map((item, index) => {
					return (
						<React.Fragment key={index}>
							<NavLink
								to={item.path}
								onClick={() => dispatch(toggleButton())}
							>
								{({ isActive }) => (
									<span
										className={`font-semi-bold  ${
											isActive
												? "font-bold text-blue-500"
												: "text-white"
										} text-24`}
									>
										{item.name}
									</span>
								)}
							</NavLink>
						</React.Fragment>
					);
				})}
			</div>
		</div>
	);
};

export default MobileNavbar;
