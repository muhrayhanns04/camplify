import DividerBox from "../../../atoms/DividerBox";
import Question from "../../../organisms/Question";
import React from "react";
import commentsData from "../../../../api/comments.json";
import fruits from "../../../../api/fruits.json";
import questionOne from "../../../../api/question_one.json";

const Content = () => {
	let data = fruits.map((e) => e.fruitName).join(", ");

	const getReplies = (data) => {
		const length = data.length;

		const replies = data.reduce((total, item) => {
			const replies = item.replies;
			if (replies) return total + getReplies(replies);
			return total;
		}, 0);

		return length + replies;
	};

	return (
		<div className="m-auto w-90% pt-64 lg:w-[895px] lg:pt-[80px]">
			<DividerBox title="One" />
			<section className="relative flex flex-col">
				{questionOne.map((item, i) => {
					return (
						<React.Fragment key={i}>
							<Question data={data} item={item} i={i} />
						</React.Fragment>
					);
				})}
			</section>
			<DividerBox title="Two" />
			<section className="relative flex flex-col">
				<p className="w-full text-left text-16 font-bold text-black">
					5. Buatlah fungsi untuk menghitung total komentar yang ada,
					termasuk semua balasan komentar. Berdasarkan data di atas,
					total komentar adalah.
				</p>
				<p className="w-full text-left font-regular text-16 text-gray-300">
					{getReplies(commentsData)} Komentar
				</p>
			</section>
		</div>
	);
};

export default Content;
